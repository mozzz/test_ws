const gulp = require('gulp');
const gutil = require('gulp-util');
const config = require('../config');
const critical = require('critical').stream;

// Generate & Inline Critical-path CSS
gulp.task('critical', function() {
  return gulp.src(config.dest.html + '/*.html').pipe(critical({
    base: config.dest.root,
    inline: true,
    css: config.dest.css + '/app.css',
    ignore: ['@font-face'],//, /url\(/
    minify: true,
    dimensions: [{
      height: 1920,
      width: 1080
    }, {
      height: 1300,
      width: 1024
    }, {
      height: 768,
      width: 768
    }, {
      height: 320,
      width: 320
    }]
  })).on('error', function(err) {
    gutil.log(gutil.colors.red(err.message));
  }).pipe(gulp.dest(config.dest.html + '/'));
});

