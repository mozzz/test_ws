import magnificPopup from 'magnific-popup';

window.magnificPopup = magnificPopup;
window.closeModal = function() {
	$.magnificPopup.close();
};
// Add it after jquery.magnific-popup.js and before first initialization code
$.extend(true, $.magnificPopup.defaults, {
	tClose: 'Закрыть (Esc)', // Alt text on close button
	tLoading: 'Loading...', // Text that is displayed during loading. Can contain %curr% and %total% keys
	gallery: {
		tPrev: 'Previous (Left arrow key)', // Alt text on left arrow
		tNext: 'Next (Right arrow key)', // Alt text on right arrow
		tCounter: '%curr% of %total%' // Markup for "1 of 7" counter
	},
	image: {
		tError: '<a href="%url%">The image</a> could not be loaded.' // Error message when image could not be loaded
	},
	ajax: {
		tError: '<a href="%url%">The content</a> could not be loaded.' // Error message when ajax request failed
	}
});
var $modals = $('.modals-holder');

$(document).on('click', '.js-modal-close', function() {
	closeModal();
});

// аякс модалки
// $('.ajax_modal').magnificPopup({
// 	type: 'ajax',
// 	removalDelay: 300,
// 	mainClass: 'my-mfp-zoom-in mo_modal',
// });
//
// $('.js-get_video').magnificPopup({
// 	type: 'iframe',
// 	removalDelay: 300,
// 	mainClass: 'my-mfp-zoom-in mo_modal',
// });
let $videoModals = $('.js-modal-video');
if ($videoModals.length > 0) {
	$videoModals.magnificPopup({
		type: 'iframe',
		removalDelay: 200,
		mainClass: 'modal-slide-up mo_modal',
	});
}

$(document).on('click', '[data-modal]', function(e) {
	e.preventDefault();
	e.stopPropagation();
	var $button = $(this).clone(),
		modalName = $button.data('modal'),
		callbacks,
		$modal = $modals.find('.js-modal_' + modalName),//.clone(),
		id;
	if (modalName === 'enroll') {
		// $modal.find('.js-master').val($button.data('id') || 0);
	} else if (modalName === 'master') {

	}
	showModal($modal);
	return false;
});

function showModal(modal, bg = true) {//ajax, callbacks
	// if (!!ajax) {
	// 	console.log('ajax');
	// 	return $.magnificPopup.open({
	// 		type: 'ajax',
	// 		items: {
	// 			src: modal,
	// 		},
	// 		ajax: {
	// 			dataType: 'html',
	// 			method: 'get',
	// 		},
	// 		closeBtnInside: true,
	// 		fixedContentPos: true,
	// 		preloader: false,
	// 		removalDelay: 300,
	// 		callbacks: callbacks || {},
	// 		mainClass: 'my-mfp-zoom-in',
	// 	});
	//
	// } else {
	// 	console.log('inline');
	// let c = $(modal).magnificPopup;
	return $.magnificPopup.open({
		type: 'inline',
		items: {
			src: modal,
		},
		closeBtnInside: true,
		fixedContentPos: true,
		preloader: false,
		removalDelay: 200,
		//callbacks: callbacks || {},
		closeOnBgClick: bg,
		// callbacks examples below
		mainClass: 'modal-slide-up mo_modal',
	});
	
	// }
	
}

window.showModal = showModal;

// let callbacksList = {
// 	beforeOpen: function() {
// 		console.log('Start of popup initialization');
// 	},
// 	elementParse: function(item) {
// 		// Function will fire for each target element
// 		// "item.el" is a target DOM element (if present)
// 		// "item.src" is a source that you may modify
//
// 		console.log('Parsing content. Item object that is being parsed:', item);
// 	},
// 	change: function() {
// 		console.log('Content changed');
// 		console.log(this.content); // Direct reference to your popup element
// 	},
// 	resize: function() {
// 		console.log('Popup resized');
// 		// resize event triggers only when height is changed or layout forced
// 	},
// 	open: function() {
// 		// console.log('Popup is opened');
// 	},
//
// 	beforeClose: function() {
// 		// Callback available since v0.9.0
// 		console.log('Popup close has been initiated');
// 	},
// 	close: function() {
// 		console.log('Popup removal initiated (after removalDelay timer finished)');
// 	},
// 	afterClose: function() {
// 		console.log('Popup is completely closed');
// 	},
//
// 	markupParse: function(template, values, item) {
// 		// Triggers each time when content of popup changes
// 		// console.log('Parsing:', template, values, item);
// 	},
// 	updateStatus: function(data) {
// 		console.log('Status changed', data);
// 		// "data" is an object that has two properties:
// 		// "data.status" - current status type, can be "loading", "error", "ready"
// 		// "data.text" - text that will be displayed (e.g. "Loading...")
// 		// you may modify this properties to change current status or its text dynamically
// 	},
// 	imageLoadComplete: function() {
// 		// fires when image in current popup finished loading
// 		// avaiable since v0.9.0
// 		console.log('Image loaded');
// 	},
//
//
// 	// Only for ajax popup type
// 	parseAjax: function(mfpResponse) {
// 		// mfpResponse.data is a "data" object from ajax "success" callback
// 		// for simple HTML file, it will be just String
// 		// You may modify it to change contents of the popup
// 		// For example, to show just #some-element:
// 		// mfpResponse.data = $(mfpResponse.data).find('#some-element');
//
// 		// mfpResponse.data must be a String or a DOM (jQuery) element
//
// 		console.log('Ajax content loaded:', mfpResponse);
// 	},
// 	ajaxContentAdded: function() {
// 		// Ajax content is loaded and appended to DOM
// 		console.log(this.content);
// 	}
// };
