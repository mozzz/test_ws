const bpList = {
	'(max-width: 1440px)': 'desktop',
	'(max-width: 1024px)': 'tablet',
	'(max-width: 640px)': 'phone-big',
	'(max-width: 320px)': 'phone',
	'(min-width: 1440px)': 'desktop-wide',
};
let current = '';

const bp = {
	previous:'',
	current: ''
};

function onResize() {
	
	for (let item in bpList) {
		if (window.matchMedia(item).matches) {
			current = bpList[item];
		}
	}
	if (bp.current !== current) {
		
		bp.previous = bp.current;
		bp.current = current;
		
		let event = new CustomEvent('bp-' + current);
		window.dispatchEvent(event);
		event = new CustomEvent('bpChanged');
		window.dispatchEvent(event);
	}
}

window.addEventListener('resize', onResize);
onResize();

module.exports = bp;
