import noUiSlider from './nouislider.js';

let sliders = document.querySelectorAll('.js-range-range');
let $sliders = $('.js-range');
$sliders.each((index,item) => {
	let $c = $(item),
		$item = $c.find('.js-range-range'),
		options = $item.data('options'),
		value = 0;
	
	noUiSlider.create($item[0], options || {});
	
	$item[0].noUiSlider.on('update',getValue);
	getValue();
	function getValue() {
		value = parseInt($item[0].noUiSlider.get());
		let query = '[data-value="' + value + '"]';
		$c.find(query).addClass('active').siblings().removeClass('active');
		
	}
});

