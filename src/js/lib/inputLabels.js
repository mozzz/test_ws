$('.js-input').focus(function() {
	$(this).parents('.l-form-item').addClass('focused');
});

$('.js-input').blur(function() {
	var inputValue = $(this).val();
	if (inputValue === '') {
		$(this).removeClass('filled');
		$(this).parents('.l-form-item').removeClass('focused');
	} else {
		$(this).addClass('filled');
	}
});
